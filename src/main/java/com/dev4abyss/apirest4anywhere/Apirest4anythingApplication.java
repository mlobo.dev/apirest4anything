package com.dev4abyss.apirest4anywhere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class Apirest4anythingApplication {

	public static void main(String[] args) {
		SpringApplication.run(Apirest4anythingApplication.class, args);
	}

}
