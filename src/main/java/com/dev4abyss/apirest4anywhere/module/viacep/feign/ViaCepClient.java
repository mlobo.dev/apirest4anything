package com.dev4abyss.apirest4anywhere.module.viacep.feign;


import com.dev4abyss.apirest4anywhere.module.viacep.dto.AddressViaCepDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "viacep", url = "${api.viacep.url}")
public interface ViaCepClient {

    @GetMapping("/{cep}/json")
    AddressViaCepDTO searchByCep(
            @PathVariable("cep") final String cep
    );
}
