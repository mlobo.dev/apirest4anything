package com.dev4abyss.apirest4anywhere.module.viacep.dto;


import lombok.Data;

@Data
public class AddressViaCepDTO {


    private String cep;
    private String logradouro;
    private String bairro;
    private String localidade;
    private String uf;
    private String complemento;
}
