package com.dev4abyss.apirest4anywhere.module.address.controller;

import com.dev4abyss.apirest4anywhere.module.viacep.dto.AddressViaCepDTO;
import com.dev4abyss.apirest4anywhere.module.viacep.feign.ViaCepClient;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Description;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
@Tag(name = "Address", description = "Address Resources")
public class AdressController {

    private final ViaCepClient viaCepClient;


    @GetMapping("/{cep}")
    @Description(value = "Search address by cep")
    public ResponseEntity<AddressViaCepDTO> findById(@PathVariable String cep) {
        return ResponseEntity.ok(viaCepClient.searchByCep(cep));
    }

}
