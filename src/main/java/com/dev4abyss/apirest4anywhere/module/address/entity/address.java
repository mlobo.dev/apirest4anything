package com.dev4abyss.apirest4anywhere.module.address.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "TB_ADDRESS")
@Entity
public class address {

    @Id
    @Column(name = "COD_ADDRESS")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "CEP")
    private String cep;

    @Column(name = "UF")
    private String uf;

    @Column(name = "CITY")
    private String city;




}
