package com.dev4abyss.apirest4anywhere.module.user.repository;

import com.dev4abyss.apirest4anywhere.module.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {


    boolean existsByEmail(String email);
}
