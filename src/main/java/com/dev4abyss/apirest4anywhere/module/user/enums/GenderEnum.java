package com.dev4abyss.apirest4anywhere.module.user.enums;

import lombok.Getter;

@Getter
public enum GenderEnum {

    MALE,
    FAMELE,
    OTHER
}
