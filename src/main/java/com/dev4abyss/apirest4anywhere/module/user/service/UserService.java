package com.dev4abyss.apirest4anywhere.module.user.service;

import com.dev4abyss.apirest4anywhere.module.exception.BusinessRuleException;
import com.dev4abyss.apirest4anywhere.module.exception.ObjectAlreadyExistsException;
import com.dev4abyss.apirest4anywhere.module.exception.ObjectNotFoundException;
import com.dev4abyss.apirest4anywhere.module.user.dto.UserDTO;
import com.dev4abyss.apirest4anywhere.module.user.entity.User;
import com.dev4abyss.apirest4anywhere.module.user.mapper.UserMapper;
import com.dev4abyss.apirest4anywhere.module.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository repository;
    private final UserMapper mapper;

    public List<User> findAll() {
        return repository.findAll();
    }

    public User findById(Long id) {
        return repository.findById(id).orElseThrow(
                () -> new ObjectNotFoundException("User not found by id: " + id)
        );
    }

    public User save(UserDTO dto) {
        if (repository.existsByEmail(dto.getEmail()))
            throw new BusinessRuleException("There is already a user registered with the email: " + dto.getEmail());

        return repository.save(mapper.toEntity(dto));
    }
}
