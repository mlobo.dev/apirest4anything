package com.dev4abyss.apirest4anywhere.module.user.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class UserDTO {

    private Long id;

    @NotNull
    @NotEmpty
    private String name;

    @Email
    private String email;

    private String password;
}
