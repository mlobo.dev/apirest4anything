package com.dev4abyss.apirest4anywhere.module.user.controller;

import com.dev4abyss.apirest4anywhere.module.user.dto.UserDTO;
import com.dev4abyss.apirest4anywhere.module.user.mapper.UserMapper;
import com.dev4abyss.apirest4anywhere.module.user.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Description;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@Tag(name = "Users", description = "Users Resources")
public class UserController {

    private final UserService service;
    private final UserMapper mapper;


    @GetMapping("/{id}")
    @Description(value = "Search an user by id")
    public ResponseEntity<UserDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.toDto(service.findById(id)));
    }

    @GetMapping
    @Description(value = "find All users")
    public ResponseEntity<List<UserDTO>> findAll() {
        return ResponseEntity.ok(mapper.toDto(service.findAll()));
    }

    @PostMapping
    @Description(value = "Save a new User")
    public ResponseEntity<UserDTO> salvar(@Validated  @RequestBody UserDTO dto) {
        return ResponseEntity.ok(mapper.toDto(service.save(dto)));
    }


}
