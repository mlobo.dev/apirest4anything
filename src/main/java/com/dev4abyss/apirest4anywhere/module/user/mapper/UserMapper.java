package com.dev4abyss.apirest4anywhere.module.user.mapper;



import com.dev4abyss.apirest4anywhere.module.base.BaseMapper;
import com.dev4abyss.apirest4anywhere.module.user.dto.UserDTO;
import com.dev4abyss.apirest4anywhere.module.user.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper extends BaseMapper<User, UserDTO> {

}
