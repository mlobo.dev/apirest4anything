package com.dev4abyss.apirest4anywhere.module.user.entity;

import com.dev4abyss.apirest4anywhere.module.user.enums.GenderEnum;
import lombok.Data;

import javax.persistence.*;

@Data
@Table(name = "TB_USER")
@Entity
public class User {

    @Id
    @Column(name = "COD_USER")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "PHONE")
    private String phone;


    @Column(name = "GENDER")
    @Enumerated(EnumType.STRING)
    private GenderEnum gender;
}
